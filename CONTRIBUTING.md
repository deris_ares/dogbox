## [Developers section below, not needed if you just want to use the software]
NodeJS is required in whatever environment you are on, either for running or for building the project

### Pre-requisite
You should download all dependencies by typing

~~~
npm install
~~~

### How to run?
You can run a SDK version in any platform by typing

~~~
npm run start
~~~

The script will download (if not present) the configured version of NW.js, the underlying technology powering DogBOX. This SDK version will include a Webkit debugger that can be raised up via F12

### How to build?
When you run the following steps you will end up with a _dist_ folder containing the binaries for the configured platforms. If you wish to publish
them as production binaries, please ZIP the corresponding folder respecting it's name, put the ZIP in [the bin folder](https://gitlab.com/deris_ares/dogbox/-/tree/master/bin) and commit it

#### Linux
~~~
npm install
npm run deploy
~~~

#### Windows
I didn't find a way for the dev dependencies to be globally accessible as a CMD command (if you did feel free to let me know), so I installed them globally. To do that, we will manually install every single dev dependency found in [package.json](https://gitlab.com/deris_ares/dogbox/-/blob/master/package.json). Chill, there are just three of them:

~~~
npm install copyfiles@^2.3.0 --global
npm install npm-run-all@^4.1.5 --global
npm install nwjs-builder-phoenix@^1.15.0 --global
~~~

Then, we can follow the same steps as Linux compilation:

~~~
npm install
npm run deploy
~~~

#### MacOS
Pending. I don't own a Mac so it's a little difficult for me to test this platform
