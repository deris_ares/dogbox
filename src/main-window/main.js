var app = angular.module('app', []);

app.controller('mainCtrl', ['$scope', '$q', 'SettingsService', function ($scope, $q, SettingsService) {
	let _settingsFile = 'settings.json'
	let _gamesFile = 'games.json'
	let _childProcess = require('child_process')
	let _path = require('path')
	
	$scope.init = function () {
		$scope.selected = [];
		
		let promises = $q.all([loadSettings(), loadGamesList()]);
		
		$scope.isReloading = true;
		
		promises.then (
			() => {
				$scope.status = makeStatus('success', 'Ready');
			},
			error => {
				$scope.status = makeStatus('error', 'Initialization failed: ' + error);
			}
			
		).finally(() => {
			$scope.isReloading = false;
		});
	};

	function loadSettings() {
		return SettingsService.load(_settingsFile).then (
			settings => {
				$scope.settings = settings;
			},

			error => {
				console.error(error);
				return $q.reject(error);
			}
		);
	}

	function loadGamesList() {
		return SettingsService.load(_gamesFile).then (
			games => {
				$scope.games = games;
			},

			error => {
				console.error(error);
				return $q.reject(error);
			}
		);
	}

	$scope.refresh = function () {

		$scope.isReloading = true;
		$scope.status = makeStatus(null, 'Games list is being reloaded...');

		return SettingsService.load(_gamesFile).then (
			function (games) {
				$scope.games = games;
				$scope.status = makeStatus('success', 'Games list has been reloaded');
			},

			function (error) {
				console.error(error);
				$scope.status = makeStatus('error', 'Games list could not be reloaded: ' + error);
			}

		).finally(function () {
			$scope.isReloading = false;
		});
	};

	function makeStatus(level, text) {
		return {level: level, text: formatDate(new Date()) + ' - ' + text};
	}

	function formatDate(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var seconds = date.getSeconds();

		if (hours < 10) {
			hours = '0' + hours;
		}

		if (minutes < 10) {
			minutes = '0' + minutes;
		}

		if (seconds < 10) {
			seconds = '0' + seconds;
		}
	
		return hours + ':' + minutes + ':' + seconds;
	}
	
	$scope.someSelected = function () {
		return 0 !== $scope.selected.length;
	};
	
	$scope.oneSelected = function () {
		return 1 === $scope.selected.length;
	};
	
	$scope.openSettings = function () {
		
		let params = {
			width: 640,
			height: 200,
			position: 'center'
		};
		
		nw.Window.open('src/settings-window/settings.html', params, newWindow => {
			newWindow.window.params = {
				settings: $scope.settings,
				saveFn: updateSettings
			};
		});
	};
	
	$scope.openCreate = function () {
		let params = {
			width: 640,
			height: 200,
			position: 'center'
		};
		
		nw.Window.open('src/edit-window/edit.html', params, newWindow => {
			newWindow.window.params = {
				saveFn: createGame
			};
		});
	};
	
	$scope.openEdit = function () {
		let params = {
			width: 640,
			height: 200,
			position: 'center'
		};
		
		nw.Window.open('src/edit-window/edit.html', params, newWindow => {
			newWindow.window.params = {
				selectedGame: $scope.selected[0],
				saveFn: updateGame
			};
		});
	};
	
	function createGame(game) {
		$scope.isSaving = true;
		
		let currentIds = $scope.games.map(each => each.id);
		let lastId = Math.max(0, ...currentIds);
		game.id = lastId + 1;
		
		let listCopy = JSON.parse(angular.toJson($scope.games)).concat(game);
		
		return saveGamesList(listCopy).finally(function () {
			$scope.isSaving = false;
		});
	}
	
	function updateGame(game) {
		$scope.isSaving = true;
		
		let listCopy = JSON.parse(angular.toJson($scope.games))
		let gameToBeSaved = listCopy.find(each => each.id === game.id)
		let promise;
		
		if (gameToBeSaved) {
			Object.assign(gameToBeSaved, game);
			promise = saveGamesList(listCopy);
		
		} else {
			promise = $q.when();
		}
		
		promise.finally(function () {
			$scope.isSaving = false;
		});
		
		return promise;
	}
	
	function saveGamesList(gamesList) {
		
		let sanitizedList = gamesList.map(each => {
			let cdISOFile = (each.cdISOFile || '').replace(/\\/g, '/');

			let ret = {
				...each,
				executable: each.executable.replace(/\\/g, '/')
			};

			if (cdISOFile) {
				ret.cdISOFile = cdISOFile;
			}

			return ret;
		})
		
		return SettingsService.save(_gamesFile, angular.toJson(sanitizedList)).then (
			function () {
				$scope.games = sanitizedList
				$scope.status = makeStatus('success', 'Games list has been saved');
			},
			
			function (error) {
				console.error(error);
				$scope.status = makeStatus('error', 'Games list could not be saved: ' + error);
			}
		);
	}
	
	function updateSettings(settings) {
		$scope.isSaving = true;
		
		let sanitizedSettings = settings.map(each => {
			return {
				...each,
				value: each.value.replace(/\\/g, '/')
			}
		});
		
		return SettingsService.save(_settingsFile, angular.toJson(sanitizedSettings)).then (
			function () {
				$scope.settings = sanitizedSettings
				$scope.status = makeStatus('success', 'Settings have been saved');
			},
			
			function (error) {
				console.error(error);
				$scope.status = makeStatus('error', 'Settings could not be saved: ' + error);
			}
		
		).finally(() => {
			$scope.isSaving = false;
		});
	}
	
	$scope.remove = function () {
		$scope.isSaving = true;
		
		let finalList = $scope.games.filter(each => !$scope.selected.includes(each))
		
		return saveGamesList(finalList).finally(function () {
			$scope.isSaving = false;
		});
	};
	
	$scope.launch = function () {
		let selectedGame = $scope.selected[0];
		let dosboxPath = $scope.settings.find(each => each.key === 'dosboxpath').value;
		let executable = `"${selectedGame.executable}"`;
		let isoMountCommand = selectedGame.cdISOFile ? `-c "imgmount d ${selectedGame.cdISOFile} -t iso"` : '';
		let allParams = [executable, isoMountCommand].filter(Boolean);
		let cmd = `"${dosboxPath}" -conf "dosbox.conf" ${allParams.join(' ')} -exit`
		let sanitizedCmd = cmd.replace(/\//g, _path.sep)
		
		_childProcess.exec(sanitizedCmd, error => {
			
			if (error) {
				console.error(error);
				$scope.status = makeStatus('error', 'Game could not be launched: ' + error);
				return;
			}
			
			$scope.status = makeStatus('success', `${selectedGame.name} has been launched`);
		})
	};

	$scope.openAbout = function () {
		let params = {
			width: 500,
			height: 375,
			position: 'center'
		};
		
		nw.Window.open('src/about-window/about.html', params);
	};

	$scope.openHowto = function () {
		let params = {
			width: 800,
			height: 600,
			position: 'center'
		};
		
		nw.Window.open('README.md', params);
	};
}]);
