angular.module('app').service('SettingsService', ['$q', function ($q) {

	this.load = function (filename) {
		var deferred = $q.defer();

		require('fs').readFile(filename, 'utf8', function (error, data) {
			if (error) {
				deferred.reject(error);

			} else {
				var settings = JSON.parse(data);
				deferred.resolve(settings);
			}
		});

		return deferred.promise;
	};

	this.save = function (filename, settings) {
		var deferred = $q.defer();

		require('fs').writeFile(filename, settings, 'utf8', function (error, data) {
			if (error) {
				deferred.reject(error);

			} else {
				deferred.resolve();
			}
		});

		return deferred.promise;
	};
}]);
