let _gui = require("nw.gui")

window.onload = () => {
  preventURLChange()
  configureExternalLinks()
}

function preventURLChange() {
  ['ondrop', 'ondragenter', 'ondragover'].forEach(each => {
    window[each] = event => event.preventDefault()
  })
}

function configureExternalLinks() {
  let externalLinks = [...document.getElementsByClassName('open-in-browser')];

  externalLinks.forEach(each => {
    each.onclick = event => {
      event.preventDefault()
      _gui.Shell.openExternal(each.getAttribute("href"))
    }
  })
}
