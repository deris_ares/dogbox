var app = angular.module('app', []);

app.controller('editCtrl', ['$scope', function ($scope) {
	let windowParams = nw.Window.get().window.params || {}
	$scope.game = { ...windowParams.selectedGame };

	$scope.saveAndClose = function () {
		windowParams.saveFn($scope.game).then(close);
	};

	function close() {
		require('nw.gui').Window.get().close();
	}
}]);
