var app = angular.module('app', []);

app.controller('settingsCtrl', ['$scope', function ($scope) {
	let windowParams = nw.Window.get().window.params || {}
	$scope.settings = windowParams.settings;

	$scope.saveAndClose = function () {
		windowParams.saveFn($scope.settings).then(close);
	};

	function close() {
		require('nw.gui').Window.get().close();
	}
}]);
