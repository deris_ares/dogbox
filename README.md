# DogBox

## What is DogBox?
DogBox is a Front-End for a MS-DOS emulation software called DOSBox, so it relies on this software to get to work. Therefore I give all credits
related with emulation mechanics to [the DOSBox crew](http://www.dosbox.com/crew.php)

This software runs under Linux and Windows. MacOS version is being worked out as well but it's not guaranteed to work since some further adjustments
are to be done yet

DogBox does the dirty work of deal with the MS-DOS system commands, and brings to the user an easy way to launch those old good MS-DOS applications
that do not run on modern operating systems. Specially, DogBox is oriented to videogames. By creating a collection of your games, you can execute them
in a single click. Forget about the command prompt!

## How to use?
A copy of DOSBox is NOT included, so you will have to to grab it on first place. You can get it as follows:

### Linux
I reccomend you to use your system's built in package manager, call it Synaptic, Discover, apt-get or whatever your system has. Just search for the package ___dosbox___ and install it

### Windows
You can download it from [here](https://www.dosbox.com/download.php?main=1). Select the Windows installer version and proceed to, well... install it

### MacOS
[Same URL](https://www.dosbox.com/download.php?main=1) as the Windows version. Choose the _Mac OS X_ link instead

### Getting started
Now you can/should download DogBox from [the bin folder](https://gitlab.com/deris_ares/dogbox/-/tree/master/bin) by choosing the proper build for your system. The first time you open DogBox, your game list will be empty and you will see an empty list (pretty obvious). The first thing to be done is to configure the path to the DOSBox we donwloaded in the previous step (you did it, right?). Click the ___Settings...___ button and place there the full
path of the DOSBox executable. In a tipycal Windows installation it will probably be _C:\Program Files (x86)\DOSBox\DOSBox.exe_. In GNU/Linux it's
likely to be _/usr/bin/dosbox_. I'm not confident about MacOS, sorry about that

Now let's add some games! Click the ___New...___ button and a small window will appear. There, you have to give a name for the game you are adding
(for example: _Prince of Persia_), and the full path to the corresponding executable file. Additionally, some games need the CD to be inserted. This
can be accomplished by providing a full path pointing to the ISO file in the last field. And how can I know which is the correct executable file?
Well, it uses to be something very close to the game title. For example: _Prince of Persia_ has a file named __prince.exe__. So clear, isn't it? Some
cases are a little less intuitive, for example _Alone in the Dark 2_, whose executable file is __AITD2.exe__, initials of _"Alone In The Dark 2"_. If
you have chosen the wrong file, the game will just not start. You can ___Edit...___ it at any time and test another one until you find the correct.
Once your game is listed, select it and press ___Launch___ to start playing it

## Troubleshooting

### I'm getting stuck after closing a game!
It's possible (specially if the game crashes or the wrong executable is chosen) that once you close a game from within its in-game menu, you fall
into a MS-DOS system with a command prompt like __C:\>__. Don't panic! You can do two things: the first one is simply type ___exit___ and press Enter.
You will get out of there. The second option is to press _Alt+Enter_ to go out of fullscreen mode. When windowed, you will be able to close that
hellborn MS-DOS prompt by clicking the __X__ at the upper corner of the window

### I'm having low FPS!
Some games, like _Alone in the Dark_ saga, will appear running at lower frames per second than you remember to have had when you played it before.
This is because DOSBox tries to determine which clock speed has to be emulated depending on many factors. But sometimes this estimation is not close
enough, beeing lower or higher than desired. The solution is, while in game, press _Ctrl+F11_ to slow down the emulation a little bit, or _Ctrl+F12_
to speed up it. You will probably have to press this combination repeatedly until you get the desired speed

But keep something in mind! If you speed it up too much to can have some issues with keys sequences. For example, in _Alone in the Dark_, a sequence
of ___UP-UP___ must be pressed to run. Internally, the game checks how many clock cycles elapsed from the first keystroke until the second one in
order to determine whether you meant to perform a double-press. If the emulation speed is very high you will have to do a lightning-speed double press
so as the system detects it, and your finger will probably die after two minutes of gameplay
